package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var jwtKey = []byte("pWAo$uaRpJIM6h48HN26VnmDwjFRlLB0O^Qn$4qP@EVL3XB*f3")

var users = map[string]string{
	"user1": "password1",
	"user2": "password2",
}

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

type claimsContextKeyType string

var claimsContextKey claimsContextKeyType = "claims"

func (c *Claims) generateNewAccessToken() (map[string]interface{}, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, c)

	tokenString, err := token.SignedString(jwtKey)

	if err != nil {
		return nil, err
	}

	data := map[string]interface{}{
		"token": tokenString,
		"data":  c,
	}

	return data, nil
}

func login(w http.ResponseWriter, r *http.Request) {
	var credentials Credentials

	w.Header().Set("Content-Type", "application/json")

	err := json.NewDecoder(r.Body).Decode(&credentials)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	expectedPassword, ok := users[credentials.Username]

	if !ok || expectedPassword != credentials.Password {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	expirationTime := time.Now().Add(5 * time.Hour)

	claims := &Claims{
		Username: credentials.Username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}

	// Attempt to generate a new token
	tokenData, err := claims.generateNewAccessToken()

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_ = json.NewEncoder(w).Encode(tokenData)
}

func welcome(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	if r.Header["Token"] == nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	tokenString := r.Header["Token"][0]

	claims := &Claims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("there was an error when signing the token")
		}

		return jwtKey, nil
	})

	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	if !token.Valid {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	_ = json.NewEncoder(w).Encode(map[string]interface{}{
		"claims": claims,
	})
}

func authenticate(next func(w http.ResponseWriter, r *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		// Check if we have the authorization headers set
		if r.Header["Authorization"] == nil {
			http.Error(w, "The Authorization header is required.", http.StatusBadRequest)
			return
		}

		// Check if the the Authorization header set is of type bearer
		header := r.Header["Authorization"]

		// Extract the token string
		splitToken := strings.Split(header[0], " ")

		// Valid we have a Bearer token
		if len(header) != 1 || splitToken[0] != "Bearer" {
			http.Error(w, "Please use Authorization Bearer header.", http.StatusBadRequest)
			return
		}

		// Init a new instance of Claims
		claims := &Claims{}

		// Parse the JWT string and store the result in claims.
		token, err := jwt.ParseWithClaims(splitToken[1], claims, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("there was an error when validating the token")
			}

			return jwtKey, nil
		})

		if err != nil {
			http.Error(w, "The provided token is invalid or it has already expired.", http.StatusUnauthorized)
			return
		}

		if !token.Valid {
			http.Error(w, "The provided token is invalid or it has already expired.", http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), claimsContextKey, claims)

		next(w, r.WithContext(ctx))
	})
}

func refresh(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Get the claims from the request context
	claims := r.Context().Value(claimsContextKey).(*Claims)

	// We ensure that a new token is not issued until enough time has elapsed
	// In this case, a new token will only be issued if the old token is within
	// 30 seconds of expiry. Otherwise, return a bad request status
	if time.Unix(claims.ExpiresAt, 0).Sub(time.Now()) > 30*time.Second {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	tokenData, err := claims.generateNewAccessToken()

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_ = json.NewEncoder(w).Encode(tokenData)
}

func main() {
	http.HandleFunc("/", login)
	http.HandleFunc("/home", welcome)
	http.Handle("/refresh", authenticate(refresh))

	fmt.Println("Server running on port 8000")
	log.Fatal(http.ListenAndServe(":8000", nil))
}
